\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{hyperref}
\hypersetup{bookmarks=true, colorlinks=false, linkcolor=blue, citecolor=blue, filecolor=blue, pagecolor=blue, urlcolor=blue,
            pdftitle=Reworking threading in GNOME Software,
            pdfauthor=Philip Withnall, pdfsubject=, pdfkeywords=}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,automata,positioning,er,calc,decorations.pathmorphing,fit}
\usepackage{listings}
\usepackage{subfigure} % can't use subfig because it doesn't support Beamer
\usepackage{fancyvrb}
\usepackage{ulem}
\usepackage{siunitx}
\usepackage[backend=biber]{biblatex}

% Syntax highlighting colours from the Tango palette: http://pbunge.crimson.ch/2008/12/tango-syntax-highlighting/
\definecolor{LightButter}{rgb}{0.98,0.91,0.31}
\definecolor{LightOrange}{rgb}{0.98,0.68,0.24}
\definecolor{LightChocolate}{rgb}{0.91,0.72,0.43}
\definecolor{LightChameleon}{rgb}{0.54,0.88,0.20}
\definecolor{LightSkyBlue}{rgb}{0.45,0.62,0.81}
\definecolor{LightPlum}{rgb}{0.68,0.50,0.66}
\definecolor{LightScarletRed}{rgb}{0.93,0.16,0.16}
\definecolor{Butter}{rgb}{0.93,0.86,0.25}
\definecolor{Orange}{rgb}{0.96,0.47,0.00}
\definecolor{Chocolate}{rgb}{0.75,0.49,0.07}
\definecolor{Chameleon}{rgb}{0.45,0.82,0.09}
\definecolor{SkyBlue}{rgb}{0.20,0.39,0.64}
\definecolor{Plum}{rgb}{0.46,0.31,0.48}
\definecolor{ScarletRed}{rgb}{0.80,0.00,0.00}
\definecolor{DarkButter}{rgb}{0.77,0.62,0.00}
\definecolor{DarkOrange}{rgb}{0.80,0.36,0.00}
\definecolor{DarkChocolate}{rgb}{0.56,0.35,0.01}
\definecolor{DarkChameleon}{rgb}{0.30,0.60,0.02}
\definecolor{DarkSkyBlue}{rgb}{0.12,0.29,0.53}
\definecolor{DarkPlum}{rgb}{0.36,0.21,0.40}
\definecolor{DarkScarletRed}{rgb}{0.64,0.00,0.00}
\definecolor{Aluminium1}{rgb}{0.93,0.93,0.92}
\definecolor{Aluminium2}{rgb}{0.82,0.84,0.81}
\definecolor{Aluminium3}{rgb}{0.73,0.74,0.71}
\definecolor{Aluminium4}{rgb}{0.53,0.54,0.52}
\definecolor{Aluminium5}{rgb}{0.33,0.34,0.32}
\definecolor{Aluminium6}{rgb}{0.18,0.20,0.21}

% Increase the space after the footnotes so that \footfullcite works
\addtobeamertemplate{footline}{\hfill\usebeamertemplate***{navigation symbols}%
    \hspace*{0.1cm}\par\vskip 20pt}{}

\usetheme{guadec}

\AtBeginSection{\frame{\sectionpage}}


% Abstract here: https://events.gnome.org/event/77/contributions/281/
%
% The 42.0 release of GNOME Software contains the first stage of a reworking of
% its internal threading. This talk will cover some of the architecture of the
% changes, the reasoning behind them, and the problems the changes intend to
% solve. The talk will also cover lessons I’ve learned from making these
% changes, and some thoughts about different approaches to using threads in
% complicated projects.
%
% Come along if you want to hear about some of the developments in GNOME
% Software 42.0; if you want to learn about how to use, abuse, or not use
% threads with GLib; or if you’d like to pick holes in this approach to landing
% large changes.


\title{Reworking threading in GNOME Software}

\author{Philip Withnall\\Endless\\\texttt{philip@tecnocode.co.uk}}
\date{July 21, 2022}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\note{25 minutes allocated. 20 minutes for talk, 5 minutes for questions.

Hello! In this talk I’m going to cover some changes we’ve been making to the
threading model in gnome-software over the last couple of cycles. Work on this
has been done by Endless and Red Hat. The aim is
both to provide a bit of a progress update for those who are interested in
gnome-software, and to provide some more general ideas about what works, and
what doesn’t work so well, when using threads in a program. I’ll also briefly
cover some thoughts about trying to minimise pain while landing big changes to
a project like this.}


\begin{frame}{History of gnome-software}
\begin{itemize}
	\item{Project started in 2012}
	\item{Plugins added in 2013}
	\item{Architecture has always been entirely plugin based}
	\item{AppStream support from 2013}
\end{itemize}
\end{frame}

\note{The gnome-software project started ten years ago. I wasn’t around then, so
please correct me if you were there any I get any of this wrong! Right from the
beginning, the architecture has been plugin based. I believe this was because
the idea was to have one or more plugins per distribution, so that the UI could
be shared between distros but it could still function with different package
managers. A lot of the original API design was done with traditional package
managers in mind, as OSTree was only a year old at the time, and it would be
another 2 years before flatpak was started.}

\note{More than just being plugin based, the architecture was \emph{entirely}
plugin based: gnome-software didn’t do anything without its plugins. AppStream
support, for example, was a plugin. Over time, the decision to standardise on
AppStream turned out to be a better and better one, but the support for it in
gnome-software remained inside a plugin just like everything else. The plugin
is mandatory now.}

\note{(AppStream is a format for providing information about software, such as
its description, screenshots, installation requirements, package format, etc.
It is distribution-agnostic. There’s plenty of documentation available about it
online: \url{https://www.freedesktop.org/software/appstream/docs/})}


\begin{frame}{Previous architecture}
\begin{figure}
	\includegraphics[width=0.9\columnwidth]{old-architecture.pdf}
	\caption{Previous architecture of gnome-software}
\end{figure}
\end{frame}

\note{In the old architecture of gnome-software, each job (such as refreshing
metadata, getting more details for applications, or installing an app) calls a
function in every plugin. All those functions are run in separate thread pool
threads --- so each job requests about 10 thread pool threads, one for each
plugin.}

\note{Plugins are run in a fixed order, defined when gnome-software starts. This
allows them to modify the results returned by earlier plugins. For example, some
plugins, such as the flatpak plugin, will add apps to the list of results for
the job. Later in the job, the icons plugin might go through the
in-progress list of results and download an icon from the internet for each
app, modifying the results list in-place.
(This isn’t shown in the diagram for reasons of space.)}

\note{Each plugin function does its work synchronously (not yielding control of
the thread until it’s finished), and then returns. When
all of the plugin functions have returned, the main thread marks the job as
complete and returns the results to the caller.}

\note{The diagram shows this as a graph of time against threads. Two jobs are
run, and cause 5 threads to be requested from the thread pool.}

\note{I’ve not shown the sequential ordering of plugins in the diagram, as
otherwise it would be too tall. However, I have shown serialisation of jobs in
separate threads due to locking inside each plugin (as outline boxes). As
functions can be called
on each plugin from multiple thread pool threads, its internal data structures
have to be locked when accessed. This serialises multiple jobs on the same
plugin, delaying subsequent ones until the first has finished.}


\begin{frame}{Issues}
\begin{enumerate}
	\item<1->{Limited number of threads in thread pool leads to exhaustion (hangs)}
	\item<2->{Large number of threads uses lots of resources (memory)}
	\item<3->{Locking required everywhere causes serialisation (slow)}
	\item<4->{Threading overhead is large for simple jobs (slow)}
\end{enumerate}
\end{frame}

\note{As the complexity of gnome-software, and the number of plugins, grew, it
became apparent that the architecture didn’t scale well enough. The most
immediate problem was that the number of threads in the thread pool is limited,
and it’s possible to hit a deadlock if the limit is reached and all the pending
threads are waiting on an operation which requires an additional thread pool
thread to complete it. This can happen with OSTree and flatpak.}

\note{One solution to that would be to increase the thread pool size limit. But
where do you stop? Every time you increase it, it’s possible to hit the deadlock
after a few more pending operations. The long-term fix is to decouple the number
of threads from the number of pending operations.}

\note{Another problem with using a lot of threads is that they consume
significant resources. While each thread theoretically shares the full address
space with the rest of the process, glibc actually reserves 64MB of heap
pages privately for each new thread (a per-thread arena
\url{https://www.gnu.org/software/libc/manual/html_node/Memory-Allocation-Tunables.html},
\url{https://siddhesh.in/posts/malloc-per-thread-arenas-in-glibc.html}.
It uses these to speed up heap allocations (\texttt{malloc()} calls)
for that thread, by avoiding locking. This is a good optimisation, but means
that the non-shareable memory use of the process increases by 64MB for each new
thread pool thread. 10 thread pool threads is not uncommon with gnome-software,
which leads to 640MB of additional non-shareable memory use. People complain
about this, which is reasonable, although not much of this
memory ever has to be physically backed.}

\note{The third and fourth problems relate to how work is scheduled on threads.
As it turns out, a lot of the work each gnome-software plugin ends up doing is
quite simple and doesn’t require much CPU time. In particular, several plugins
just talk to a daemon over D-Bus, and don’t do any CPU intensive work
themselves.}


\begin{frame}{GDBus threading}
\begin{figure}
	\includegraphics[width=0.9\columnwidth]{gdbus-worker.pdf}
	\caption{Threading when making a GDBus call}
\end{figure}
\end{frame}

\note{In order to do this, they use GDBus. Whenever you make a method call in
GDBus, the calling thread puts together a D-Bus message packet, and then
queues it to a GDBus worker thread provided by GLib. That worker thread handles
communicating with the daemon, and processing replies from it. So when a
gnome-software plugin was making a D-Bus call, a thread pool thread was being
picked, sent a small message about the ongoing job, which was then doing a small
amount of work to turn that into a D-Bus call, and sending that message on to
the GDBus worker thread. Until the daemon and GDBus worker thread replied, the
thread pool thread then slept and didn’t do any other work.}

\note{This occupied thread pool threads for a relatively long time (the time
taken for the daemon to do whatever it was asked to do, plus the D-Bus round
trip time of on the order of 1ms), while actually spending very little time
using the CPU or other resources. When thread pool threads are a scant resource,
this is not an efficient use of them.}


\begin{frame}{Previous architecture}
\begin{figure}
	\includegraphics[width=0.9\columnwidth]{old-architecture.pdf}
	\caption{Previous architecture of gnome-software}
\end{figure}
\end{frame}

\note{Recap of the old architecture.}


\begin{frame}{New architecture}
\begin{figure}
	\includegraphics[width=0.85\columnwidth]{new-architecture.pdf}
	\caption{New architecture of gnome-software}
\end{figure}
\end{frame}

\note{So this brings us to the new architecture. The key change is that plugins
are now in charge of their own threading model rather than being forced to
use a thread pool thread for each operation.}

\note{This means that some plugins can work entirely in the main thread, without
using any separate threads, as the work they do is light and doesn’t block. You
can see that with this short function call for job 2, now run in the main thread.
Other plugins, which do need to do blocking operations, can explicitly run their
own worker thread (or threads). This also allows them to set appropriate
scheduling priorities for the thread. The \texttt{appstream} and
\texttt{flatpak} plugins do this.}

\note{By using a fixed number of threads per plugin, the problem of exhausting
the thread pool is eliminated --- the width of this graph is now bounded.}

\note{The plugins which now operate without a thread no longer need to do any
locking, which means that concurrent jobs on them won’t be serialised and block
each other. Locking and serialisation are still required for the plugins which
have threads, though. That’s unavoidable.}

\note{Using fewer threads also means a reduction in the non-shared memory
footprint of gnome-software. There is still more work to do in this area,
though, as further memory reductions are possible through other changes to the
code unrelated to threading.}

\note{The new way for plugins to make D-Bus calls is to just make them
asychronously from the main thread. This means the D-Bus message is assembled in
the main thread (which doesn’t take long), enqueued to the GDBus worker thread
(which always exists, provided by GLib),
and then the main thread gets on with other work until there’s a callback from
the GDBus worker with a reply.}

\note{As well as reducing the use of thread pool threads, this makes it very
easy for a plugin to have multiple D-Bus calls in flight at once. Previously,
the easiest way to code a plugin was to serialise D-Bus calls. Parallelising
them may speed up some plugin operations (if the daemon the plugin is talking
to can handle the calls faster in parallel). This is certainly the case in the
PackageKit plugin.}

\note{Finally, changing the architecture has allowed us to split plugin jobs
up into one class per job, rather than one generic job class used for all jobs.
For example, there’s now \texttt{GsPluginJobListApps} for querying for apps,
and \texttt{GsPluginJobRefreshMetadata} for refreshing repo metadata. This
makes the code a lot more type safe and hence understandable: rather than having
to remember which job properties apply to each job, the API specifies it. It
also allows expanding the API to expose, for example, job-specific and more
detailed progress reporting in future.}


\begin{frame}{Different approaches to threading in C}
\begin{itemize}
	\item{Synchronous code always run in a worker thread}
	\item{Asynchronous code run somewhere}
	\item{Threading model determined at a high level vs locally}
\end{itemize}
\end{frame}

\note{This is the bit of the talk which is going to be a bit more like a
general software engineering textbook. What are the trade-offs when writing
threading in C in the GNOME environment? The trade-offs with higher level
languages or with other toolkits might be different, as they provide different
primitives and language constructs which help you in different ways. Using a
higher level or more modern language than C is the best way to go if you have
the choice. But there are various places in GNOME where, due to the rest of the
ecosystem being in C, it is pragmatic to still write new code in C. In these
cases, you basically have the choice between writing synchronous code to run in
a separate thread, or writing asynchronous code to run somewhere --- in the main
thread or in a separate thread.}

\note{Writing synchronous code is easy, and it’s easy to read sequentially. As
a result, it’s easier to maintain. Writing asynchronous code requires more
boilerplate in C, and is thus a bit harder to maintain. The main benefit that
asynchronous code provides, however, is that it defines the yield points in
code: where the code needs to block on I/O or other synchronisation, and hence
where it can yield control of the thread to something else for a while. It’s not
possible to define these in synchronous
code in C --- there is no \texttt{yield} keyword.}

\note{The fact that its yield points are defined means that asynchronous code
can be run in a worker thread or in the
main thread fairly easily. It can be run synchronously using a trivial
async-to-sync wrapper. For that reason, I think it’s better to define all APIs
as asynchronous, even if the initial implementation of them is written
synchronously for ease of development. (Or: just use a higher level language.)}

\note{gnome-software didn’t do that: its plugin job API was synchronous, and
hence plugins didn’t have the choice of running in the main thread. They were
all bound to always being run in thread pool threads. And while that allowed for
fast development and linear code, it committed the code to high thread use.
It’s worth noting that a lot of these asynchronous coding patterns were refined
since gnome-software was written, so gnome-software is basically just a victim
of its age.}

\note{Further reading about threads and, in particular, their interactions with
main contexts, is here: \url{https://developer.gnome.org/ExtractShell/documentation/tutorials/threading.html}.}


\begin{frame}{Approaches for landing big changesets}
\begin{itemize}
	\item{Land early}
	\item{Keep things working}
	\item{Keep adapter wrappers around old code and drop it eventually}
\end{itemize}
\end{frame}

\note{So how have we gone about landing these changes? From my point of view,
development has worked very well. Others might give you a different perspective
though. This series of changes to gnome-software is quite large --- we’re now up
to about 30 merge requests. One thing that seems to have worked well is to land
smallish chunks of work fairly often, and keep close to \texttt{main}.}

\note{By keeping merge requests small, they’ve been easier to review, and have
had minimal merge conflicts. The code which has landed early has been tested in
\texttt{main} for longer, and bugs which have been found in it have informed
subsequent development.}

\note{The downside to this is that old code -- which we’re trying to
replace -- hangs around for longer and gets
gnarlier: 30 merge requests into the refactor, the \texttt{GsPluginLoader} code
has gained a load of new exceptions to interface it with the new code, and only
a few parts of the old code have been dropped at this point. You don’t get the
pleasure of seeing a huge diffstat for all the changes.}

\note{The other advantage of landing smaller chunks of work often is that it
allows the work to be split up, with multiple people contributing different
parts of the refactor in parallel at times.}

\note{A big thank you to Milan Crha, Georges Neto and everyone else who’s been
involved in doing and reviewing the threading rework in gnome-software. It’s
very much been a collaborative project.}


\begin{frame}{Miscellany}
\begin{description}
	\item[Slide source]{\url{https://gitlab.com/pwithnall/guadec-gnome-software-presentation-2022}}
	\item[gnome-software project]{\url{https://gitlab.gnome.org/GNOME/gnome-software}}
\end{description}


% Creative commons logos from http://blog.hartwork.org/?p=52
\vfill
\begin{center}
	\includegraphics[scale=0.83]{cc_by_30.pdf}\hspace*{0.95ex}\includegraphics[scale=0.83]{cc_sa_30.pdf}\\[1.5ex]
	{\tiny\textit{Creative Commons Attribution-ShareAlike 4.0 International License}}\\[1.5ex]
	\vspace*{-2.5ex}
\end{center}

\vfill
\begin{center}
	\tiny{Beamer theme: \url{https://gitlab.gnome.org/GNOME/presentation-templates/tree/master/GUADEC/2022}}
\end{center}
\end{frame}

\end{document}
